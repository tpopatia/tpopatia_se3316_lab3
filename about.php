<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>tpopatia Lab3 SE3316A</title>
    
  <link href="https://fonts.googleapis.com/css?family=Cuprum|Cookie" rel="stylesheet" type="text/css">  

  <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  <script language="JavaScript" type="text/javascript" src="jquery.js"></script>
  <link href="Lab3_Resources/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="Lab3_Resources/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Lab3_Resources/bootstrap/css/bootstrap-theme.min.css">
  <script src="Lab3_Resources/bootstrap/js/bootstrap.min.js"></script>
  <link href="lab3.css" rel="stylesheet">
  
    
    </head>
    
    <body>
        <header>

           <div id="topHeaderRow" >
              <div class="container">
                 <nav class="navbar navbar-inverse " role="navigation">
                    <div class="navbar-header">
                       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                       </button>
                       <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
                    </div>
        
                    <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                       <ul class="nav navbar-nav">
                          <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                       </ul>
                    </div>  <!-- end .collpase --> 
                 </nav>  <!-- end .navbar --> 
              </div>  <!-- end .container --> 
           </div>  <!-- end #topHeaderRow --> 
           
           <div id="logoRow" >
              <div class="container">
                 <div class="row">
                    <div class="col-md-8">
                        <h1>Art Store</h1> 
                    </div>
                    
                    <div class="col-md-4">
                       <form class="form-inline" role="search">
                          <div class="input-group">
                             <label class="sr-only" for="search">Search</label>
                             <input type="text" class="form-control" placeholder="Search" name="search">
                             <span class="input-group-btn">
                             <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                             </span>
                          </div>
                       </form> 
                    </div>   <!-- end .navbar --> 
                 </div>   <!-- end .row -->        
              </div>  <!-- end .container --> 
           </div>  <!-- end #logoRow --> 
           
           <div id="mainNavigationRow" >
              <div class="container">
        
                 <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                       </button>
                    </div>
        
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                     <ul class="nav navbar-nav">
                       <li><a href="index.php">Home</a></li>
                       <li class="active"><a href="about.php">About Us</a></li>
                       <li><a href="work.php">Art Works</a></li>
                       <li ><a href="artists.php">Artists</a></li>
                       <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                           <li><a href="#">Special 1</a></li>
                           <li><a href="#">Special 2</a></li>                   
                         </ul>
                       </li>
                     </ul>              
                    </div>
                 </nav>  <!-- end .navbar --> 
              </div>  <!-- end container -->
           </div>  <!-- end mainNavigationRow -->
   
        </header>

            <div class="container">
              <div class="jumbotron">
                <h2>About Us</h2>
                <p>This assignment was created by: </br> </br>
                <em>Tabriz Popatia (tpopatia@uwo.ca)</em> </p> </br>
                <p>It was created for SE 3316A at Western University.</p>
                <p>
                  <a class="btn btn-lg btn-primary" href="http://owl.uwo.ca" role="button">Learn more</a>
                </p>
              </div>
            </div>  
            
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
          <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>