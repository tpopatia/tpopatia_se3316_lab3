
<html lang = en>
  <head>
  <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>tpopatia Lab3 SE3316A</title>
    
  <link href="https://fonts.googleapis.com/css?family=Cuprum|Cookie" rel="stylesheet" type="text/css">  

   <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  <script language="JavaScript" type="text/javascript" src="jquery.js"></script>
  
  <link href="Lab3_Resources/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="Lab3_Resources/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Lab3_Resources/bootstrap/css/bootstrap-theme.min.css">
  <script src="Lab3_Resources/bootstrap/js/bootstrap.min.js"></script>
  
 
  <link href="carousel.css" rel="stylesheet">
  
  <script language="JavaScript" type="text/javascript">
    $(document).ready(function(){
      $('.myCarousel').carousel({
          interval: 500
      })
      });    
      </script>
      
      <?php
        $paintings = file("Lab3_Resources/data-files/paintings.txt");
        $painting = array();
       
        
        foreach ($paintings as $counter => $paintingInfo){
          $paintingData = explode("~",$paintingInfo);
        
          
          foreach($paintingData as $i => $data){
            $painting[$counter][$i] = $data;
        
          }
        
        }
        
        
      ?>
         
  </head>
    
  <body>
    <div class="navbar-wrapper">
      <div class="container">
        <div class="navbar navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">LAB 3</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="work.php">Work</a></li>  
                <li><a href="artists.php">Artists</a></li>                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Carousel -------------------------------------------------------------->
     <div id="myCarousel" class="carousel slide" data-ride="carousel">
       
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
     <div class="carousel-inner">
      
      <!------inner Carousel------------------->
      
      <?php
      for($i =0; $i<5; $i++){
        if($i==0)
        echo '<div class="item active">';
        
        else
        echo '<div class = "item">';
        
        echo
        '<img src="Lab3_Resources/art-images/paintings/medium/'.$painting[$i][3].'.jpg" alt="'.$painting[$i][4].'" title="'.$painting[$i][4].'" rel="#PaintingThumb" /> 
         <div class="container">
          <div class="carousel-caption">
            <h1>'.$painting[$i][4].'</h1>
            <p>'.$painting[$i][6].'</p>
            <p><a class="btn btn-lg btn-primary" href="'.$painting[$i][12].'" role="button">Learn more</a></p>
          </div>
        </div>
      </div>';
        
      }
      
      ?>
       
       
     </div><!-- Close carouel-inner -->
     
     <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
       <span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span></a>
     
    </div><!-- Close carousel -->
    
    <!-- Under Carousel -------------------------------------->
    <div class="container marketing">
      
    <?php
      for($i = 5; $i<11;$i++){
        $description = explode(".",$painting[$i][5]);
        if(($i+1)%3==0)
          echo '<div class="row">';
          
        echo '<div class="col-lg-4">
        <img class="img-circle" src="Lab3_Resources/art-images/paintings/medium/'.$painting[$i][3].'.jpg" alt="'.$painting[$i][4].'" title ="'.$painting[$i][4].'"style="width:100px; height:100px;" /> 
        <h2>'.$painting[$i][4].'</h2>
        <p class="text-justify">'.$description[0].'.</p>
        <p><a class="btn btn-default" href="'.$painting[$i][12].'" role="button">View details &raquo;</a></p>
        </div>';
        
         if(($i-1)%3==0)
          echo '</div>';
      }
    
    ?>
    
    </div>
    
     <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Lab3_Resources/bootstrap/js/bootstrap.min.js"></script>
   
    
    </body>
</html>