<html lang = en>
  <head>
  <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>tpopatia Lab3 SE3316A</title>
    
  <link href="https://fonts.googleapis.com/css?family=Cuprum|Cookie" rel="stylesheet" type="text/css">  

  <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  <script language="JavaScript" type="text/javascript" src="jquery.js"></script>
  <link href="Lab3_Resources/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="Lab3_Resources/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Lab3_Resources/bootstrap/css/bootstrap-theme.min.css">
  <script src="Lab3_Resources/bootstrap/js/bootstrap.min.js"></script>
  
  <link href="carousel.css" rel="stylesheet">
  <link href ="lab3.css" rel ="stylesheet">
  
  <script language="JavaScript" type="text/javascript">
    $(document).ready(function(){
      $('.myCarousel').carousel({
          interval: 5000
      })
      });
      $(document).ready(function(){
      $('.dropdown-toggle').dropdown()
      });
      </script>
      
      <?php
        $paintings = file("Lab3_Resources/data-files/paintings.txt");
        $painting = array();
        $counter = 0;
        
        foreach ($paintings as $paintingInfo){
          $paintingData = explode("~",$paintingInfo);
          $i = 0;
          
          foreach($paintingData as $data){
            $painting[$counter][$i] = $data;
            $i++;
          }
          $counter++; 
        }
        
        
      ?>
         
  </head>
    
  <body>
    
    <header>
  
     <div id="topHeaderRow">
        <div class="container">
           <nav role="navigation" class="navbar navbar-inverse ">
              <div class="navbar-header">
                 <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                 </button>
                 <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a class="navbar-link" href="#">Login</a> or <a class="navbar-link" href="#">Create new account</a></p>
              </div>
  
              <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                 <ul class="nav navbar-nav">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                 </ul>
              </div>  <!-- end .collpase --> 
           </nav>  <!-- end .navbar --> 
        </div>  <!-- end .container --> 
     </div>  <!-- end #topHeaderRow --> 
     
     <div id="logoRow">
        <div class="container">
           <div class="row">
              <div class="col-md-8">
                  <h1>Art Store</h1> 
              </div>
              
              <div class="col-md-4">
                 <form role="search" class="form-inline">
                    <div class="input-group">
                       <label for="search" class="sr-only">Search</label>
                       <input type="text" name="search" placeholder="Search" class="form-control">
                       <span class="input-group-btn">
                       <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                       </span>
                    </div>
                 </form> 
              </div>   <!-- end .navbar --> 
           </div>   <!-- end .row -->        
        </div>  <!-- end .container --> 
     </div>  <!-- end #logoRow --> 
     
     <div id="mainNavigationRow">
        <div class="container">
  
           <nav role="navigation" class="navbar navbar-default">
              <div class="navbar-header">
                 <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                 </button>
              </div>
  
              <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav">
                 <li><a href="index.php">Home</a></li>
                 <li><a href="about.php">About Us</a></li>
                 <li class="active"><a href="#">Art Works</a></li>
                 <li><a href="artists.php">Artists</a></li>
                 <li class="dropdown">
                   <a data-toggle="dropdown" class="dropdown-toggle" href="#">Specials <b class="caret"></b></a>
                   <ul class="dropdown-menu">
                     <li><a href="#">Special 1</a></li>
                     <li><a href="#">Special 2</a></li>                   
                   </ul>
                 </li>
               </ul>              
              </div>
           </nav>  <!-- end .navbar --> 
        </div>  <!-- end container -->
       </div>  <!-- end mainNavigationRow -->
     
    </header>
    
    <div class="container">
    <div class="row">
      <!--------------------Carousel----------------------->
      
      <div id="myCarousel" class="carousel slide" data-ride="carousel">

     
      <div class="carousel-inner">
      <?php
     
      foreach($painting as $i => $paintingInfo){
        if($i == 0)
          echo '<div class="item active">';
        else 
          echo '<div class="item">';
          
              echo ' <div class="container">
                        <div class="col-md-10">
                            <h2>'.$paintingInfo[4].'</h2>
                            <p><a href="#">'.$paintingInfo[6].'</a></p>
                            <div class="row">
                                <div class="col-md-5"><img class="img-thumbnail img-responsive" src="Lab3_Resources/art-images/paintings/medium/'.$paintingInfo[3].'.jpg" alt="'.$paintingInfo[4].'" title="'.$paintingInfo[4].'"/></div>
                            <div class="col-md-7">
                                <p style="text-align: justify; 	text-justify: inter-word;">'.$paintingInfo[4].'</em>'.$paintingInfo[5].'</p>
                                <p class="price">'.$paintingInfo[11].'</p>
                                <div class="btn-group btn-group-lg">
                                    <button class="btn btn-default" type="button">
                                    <a href="#"><span class="glyphicon glyphicon-gift"></span> Add to Wish List</a>  
                                    </button>
                                    <button class="btn btn-default" type="button">
                                        <a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Add to Shopping Cart</a>
                                    </button>
                                </div>               
                                <p>&nbsp;</p>
                                <div class="panel panel-default">
                                    <div class="panel-heading">Product Details</div>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Date: </th>
                                                    <td>'.$paintingInfo[6].'</td>
                                                </tr>
                                                <tr>
                                                    <th>Medium:</th>
                                                    <td>'.$paintingInfo[9].
                                                '</td>
                                                </tr>  
                                                <tr>
                                                    <th>Dimensions:</th>
                                                    <td> '.$paintingInfo[7].' x '.$paintingInfo[8].'</td>
                                                </tr> 
                                                <tr>
                                                    <th>Home:</th>
                                                    <td><a href="#">'.$paintingInfo[10].'</a></td>
                                                </tr>  
                                                <tr>
                                                    <th>Link:</th>
                                                    <td><a href="'.$paintingInfo[12].'">Wiki</a></td>
                                                </tr>     
                                         </tbody></table>
                                    </div>                              
                   
                                </div>  
                            </div>  
                        </div>
                    </div>
                </div>';
                 
      }
      
      ?>
   
      
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
       <span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span></a>
      </div>
      
    </div>
    </div>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Lab3_Resources/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>